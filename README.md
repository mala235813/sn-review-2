# /sn

`sn` is a bare-bones, proof-of-concept, terminal-based social network based on `git `. It is created for the Final Assignment of the [Unix Tools](https://courses.edx.org/courses/course-v1:DelftX+UnixTx+1T2020/course/) course on edX and is loosely based on principles from [twtxt](https://twtxt.readthedocs.io/en/stable/user/intro.html).

## Installation

Extract the contents, and run the command `./sn` from your terminal. Each command has instructions if you don't specify any arguments.


## Basic Features

- [x] sn create: Create a new social network
- [ ] sn join: Join an existing social network by cloning it to your PC
- [ ] sn pull: Pull in new posts and likes
- [x] sn log: Show a list of existing posts
- [x] sn show: Show a specified post
- [x] sn post: Post a new story
- [x] sn like: Like a specified post
- [ ] sn push: Push locally made changes back to the server
- [x] sn members: Show the network's members
- [x] sn follow: Follow the posts of the specified member
- [x] sn unfollow: Unfollow the posts of the specified member


## Additional Features (TODO)

1. [x] sn reset: Delete all networks and reset
2. [x] sn likes: Show liked postsS
3. [x] sn switch: Switch active instance
4. [x] sn feed: Show x posts from followed users
5. [x] sn followers: Show people followed by
6. [ ] sn following: Show people following, or followed


---

[MIT Licence](LICENCE.txt)